import React from 'react';
import classNames from 'classnames';
import {
  BTN_CLASS,
  NAV_BTN_CLASS,
  BTN_SELECTED_CLASS,
  BTN_DISABLED_CLASS,
} from './Constants';

const WizardNavigation = (props) => {
  const { steps, currentPage, jumpToPage, lastVisitedPage } = props;

  return steps.map((step, index) => {
    const classForBtn = classNames(BTN_CLASS, NAV_BTN_CLASS, {
      [BTN_SELECTED_CLASS]: index === currentPage - 1,
      [BTN_DISABLED_CLASS]: index > lastVisitedPage - 1,
    });

    return (
      <button
        key={index}
        className={classForBtn}
        onClick={jumpToPage.bind(null, index + 1)}
      >
        {step}
      </button>
    );
  });
};

export default WizardNavigation;
