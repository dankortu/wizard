import React from 'react';

const UserData = (props) => {
  const { handler, valueInputName, valueInputLastname } = props;

  return (
    <div className="user-data">
      <h1>Введите данные</h1>

      <div className="name field">
        <p>Имя:</p>{' '}
        <input
          name="name"
          type="text"
          placeholder="Вася"
          tabIndex="0"
          onChange={handler}
          value={valueInputName}
          autoFocus
        />
      </div>
      <div className="lastname field">
        <p>Фамилия:</p>{' '}
        <input
          name="lastname"
          type="text"
          placeholder="Уткин"
          onChange={handler}
          value={valueInputLastname}
        />
      </div>
    </div>
  );
};

export default UserData;
