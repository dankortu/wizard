export const INVISIBLE_CLASS = 'invisible';
export const BTN_CLASS = 'btn';
export const NAV_BTN_CLASS = 'nav-btn';
export const BTN_SELECTED_CLASS = 'btn-selected';
export const BTN_DISABLED_CLASS = 'btn-disabled';
export const CONTROL_PANEL_CLASS = 'control-panel';
export const CONTROL_PANEL_BTN_CENTER_CLASS = 'control-panel-center';

export const REGISTER_BUTTON_PAGE_NUM = 0;
export const USER_DATA_PAGE_NUM = 1;
export const CONTACTS_PAGE_NUM = 2;
export const CARD_PAGE_NUM = 3;
export const PASSWORD_PAGE_NUM = 4;
export const CONGRATULATIONS_PAGE_NUM = 5;

export const REG_NAME = /^[a-zа-яё-]+$/i;
export const REG_PHONE = /^((8|\+7)[- ]?)?(\(?\d{3}\)?[- ]?)?[\d\- ]{7,10}$/;
export const REG_EMAIL = /[a-z0-9]+@[a-z0-9]+\.[a-z0-9]+/i;
export const REG_CARD = /_/;
