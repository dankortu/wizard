import React from 'react';
import classNames from 'classnames';
import {
  CONTROL_PANEL_BTN_CENTER_CLASS,
  CONTROL_PANEL_CLASS,
  BTN_CLASS,
  INVISIBLE_CLASS,
  REGISTER_BUTTON_PAGE_NUM,
  USER_DATA_PAGE_NUM,
} from './Constants';

const Buttons = (props) => {
  const { nextPage, prevPage, pageNumber } = props;
  const classForPrevPageBtn = classNames(BTN_CLASS, {
    [INVISIBLE_CLASS]: pageNumber == USER_DATA_PAGE_NUM,
  });

  const classForControlPanel = classNames([CONTROL_PANEL_CLASS], {
    [CONTROL_PANEL_BTN_CENTER_CLASS]: pageNumber == USER_DATA_PAGE_NUM,
    [INVISIBLE_CLASS]: pageNumber == REGISTER_BUTTON_PAGE_NUM,
  });
  return (
    <div className={classForControlPanel}>
      <button className={classForPrevPageBtn} onClick={prevPage}>
        Назад
      </button>
      <button className="btn next-page" onClick={nextPage}>
        Далее
      </button>
    </div>
  );
};

export default Buttons;
