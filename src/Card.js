import React from 'react';
import InputMask from 'react-input-mask';

const Card = (props) => {
  const { handler, valueInputCard } = props;

  return (
    <div className="user-data">
      <h1>Введите данные</h1>

      <div className="card field">
        <p>Номер карты: </p>
        <InputMask
          name="card"
          mask="9999 9999 9999 9999"
          value={valueInputCard}
          onChange={handler}
          autoFocus
        ></InputMask>
      </div>
    </div>
  );
};

export default Card;
