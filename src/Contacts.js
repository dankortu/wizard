import React from 'react';
import InputMask from 'react-input-mask';

const Contacts = (props) => {
  const { handler, valueInputPhone, valueInputEmail } = props;

  return (
    <div className="user-data">
      <h1>Введите данные</h1>

      <div className="phone field">
        <p>Телефон:</p>
        <InputMask
          name="phone"
          mask="+7 (999) 999-99-99"
          value={valueInputPhone}
          onChange={handler}
          autoFocus
        ></InputMask>
      </div>

      <div className="email field">
        <p>Email:</p>
        <input
          name="email"
          type="email"
          placeholder="email@exaple.com"
          onChange={handler}
          value={valueInputEmail}
        />
      </div>
    </div>
  );
};

export default Contacts;
